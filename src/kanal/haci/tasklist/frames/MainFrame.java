package kanal.haci.tasklist.frames;

import java.awt.EventQueue;

import javax.swing.JFrame;

import kanal.haci.tasklist.core.custom.ui.MainFrameGui;

public class MainFrame {

	public static JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("static-access")
			public void run() {
				try {
					MainFrame window = new MainFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() {
		initialize();
	}

	private void initialize() {
		MainFrameGui.init();
	}

}
