package kanal.haci.tasklist.frames;

import java.awt.EventQueue;

import javax.swing.JFrame;

import kanal.haci.tasklist.core.custom.ui.ListFrameGui;

public class ListFrame {

	public static JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("static-access")
			public void run() {
				try {
					ListFrame window = new ListFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ListFrame() {
		initialize();
	}

	private void initialize() {
		ListFrameGui.init();
		ListFrameGui.tableDoubleClickListener();
	}

}
