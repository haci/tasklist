package kanal.haci.tasklist.hashmap;

import java.util.HashMap;

public class NestedHashMap<K1, K2, V> extends HashMap<K1, HashMap<K2, V>> {

	private static final long serialVersionUID = 1L;

	public NestedHashMap() {
		super();
	}

	public void putChildrenValue(K1 key1, K2 key2, V value) {

		HashMap<K2, V> childMap = get(key2);
		if (childMap == null) {
			childMap = new HashMap<K2, V>();
			put(key1, childMap);
		}
		childMap.put(key2, value);
	}
}
