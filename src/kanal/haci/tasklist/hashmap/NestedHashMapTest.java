package kanal.haci.tasklist.hashmap;

public class NestedHashMapTest {

	public static void main(String[] args) {
		NestedHashMap<String, Integer, String> map = new NestedHashMap<>();
		map.putChildrenValue("Ahmet", 5248, "Turkish");
		map.putChildrenValue("Mehmet", 1234, "Turkish");

		System.out.println(map.get("Ahmet"));
		System.out.println(map.get("Mehmet"));

	}
}
