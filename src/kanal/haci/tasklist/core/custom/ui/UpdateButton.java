package kanal.haci.tasklist.core.custom.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import kanal.haci.tasklist.core.connection.DBManager;
import kanal.haci.tasklist.core.connection.DBTransactionMessages;

public class UpdateButton extends JButton {

	private DBManager dbManager = new DBManager();

	private static final long serialVersionUID = 1L;

	public UpdateButton(String text) {
		setText(text);
		clickListener();
	}

	private void clickListener() {
		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					dbManager.updateRecord("UPDATE user_datas SET name = '" + PopUpMenu.txtName.getText().toString()
							+ "', surname = '" + PopUpMenu.txtSurname.getText().toString() + "' WHERE id = '"
							+ PopUpMenu.userId + "' ");
					JOptionPane.showMessageDialog(null, DBTransactionMessages.UPDATE);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

			}
		});
	}
}
