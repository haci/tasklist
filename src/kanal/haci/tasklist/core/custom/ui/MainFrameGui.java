package kanal.haci.tasklist.core.custom.ui;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import kanal.haci.tasklist.frames.MainFrame;
import kanal.haci.tasklist.numeric.textbox.NumText;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

public class MainFrameGui {

	public static JTextField txtName;
	public static JTextField txtSurname;
	public static JCheckBox chckbxMale;
	public static JCheckBox chckbxFemale;
	public static SaveButton btnSave;

	public MainFrameGui() {
	}

	public static void init() {
		MainFrame.frame = new JFrame();
		MainFrame.frame.setBounds(100, 100, 450, 300);
		MainFrame.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MainFrame.frame.getContentPane().setLayout(
				new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblName = new JLabel("Name:");
		MainFrame.frame.getContentPane().add(lblName, "2, 4, right, default");

		txtName = new JTextField();
		MainFrame.frame.getContentPane().add(txtName, "4, 4, left, default");
		txtName.setColumns(10);

		JLabel lblSurname = new JLabel("Surname:");
		MainFrame.frame.getContentPane().add(lblSurname, "2, 6, right, default");

		txtSurname = new JTextField();
		MainFrame.frame.getContentPane().add(txtSurname, "4, 6, left, default");
		txtSurname.setColumns(10);

		JLabel lblGender = new JLabel("Gender:");
		MainFrame.frame.getContentPane().add(lblGender, "2, 8, right, default");

		chckbxMale = new JCheckBox("Male");
		chckbxMale.setSelected(true);
		MainFrame.frame.getContentPane().add(chckbxMale, "4, 8");

		chckbxFemale = new JCheckBox("Female");
		MainFrame.frame.getContentPane().add(chckbxFemale, "4, 10");

		btnSave = new SaveButton("Save");
		MainFrame.frame.getContentPane().add(btnSave, "4, 12, left, default");

		JLabel lblNumText = new JLabel("I Only Accept Numbers");
		MainFrame.frame.getContentPane().add(lblNumText, "4, 4, center, default");

		NumText numText = new NumText();
		MainFrame.frame.getContentPane().add(numText, "4, 6, center, default");
		numText.setColumns(10);

		chckboxController();
	}

	public static void chckboxController() {
		chckbxMale.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (chckbxMale.isSelected()) {
					chckbxFemale.setSelected(false);
				}
			}
		});
		chckbxFemale.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (chckbxFemale.isSelected()) {
					chckbxMale.setSelected(false);
				}
			}
		});
	}

}
