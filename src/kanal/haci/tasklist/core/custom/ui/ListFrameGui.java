package kanal.haci.tasklist.core.custom.ui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import kanal.haci.tasklist.frames.ListFrame;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

public class ListFrameGui {

	public static JTable table;
	public static ShowButton btnShow;
	public static TableModel model;

	public ListFrameGui() {
	}

	public static void init() {

		ListFrame.frame = new JFrame();
		ListFrame.frame.setBounds(100, 100, 450, 300);
		ListFrame.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ListFrame.frame.getContentPane().setLayout(
				new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						RowSpec.decode("default:grow"), }));

		btnShow = new ShowButton("Show");
		ListFrame.frame.getContentPane().add(btnShow, "1, 2, 18, 1");
		table = new JTable();
		String headers[] = { "ID", "NAME", "SURNAME", "GENDER" };
		model = new DefaultTableModel(headers, 0);
		table.setModel(model);
		ListFrame.frame.getContentPane().add(table, "1, 8, 18, 1, fill, fill");
	}

	public static void tableDoubleClickListener() {
		table.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {

			}

			@Override
			public void mousePressed(MouseEvent e) {

			}

			@Override
			public void mouseExited(MouseEvent e) {

			}

			@Override
			public void mouseEntered(MouseEvent e) {

			}

			@Override
			public void mouseClicked(MouseEvent e) {

				if (e.getClickCount() == 2) {
					int row = table.getSelectedRow();
					if (row >= 0) {
						String id = String.valueOf(table.getValueAt(row, 0));
						String name = String.valueOf(table.getValueAt(row, 1));
						String surname = String.valueOf(table.getValueAt(row, 2));

						PopUpMenu.showPopUpMenu(id, name, surname);
						
					}
				}
			}
		});
	}

}
