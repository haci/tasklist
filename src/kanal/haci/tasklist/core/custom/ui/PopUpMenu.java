package kanal.haci.tasklist.core.custom.ui;

import java.awt.GridLayout;

import javax.swing.*;

public class PopUpMenu {

	private static JPanel panel;
	private static UpdateButton updateButton;
	private static DeleteButton deleteButton;

	public static int userId;
	public static JTextField txtName;
	public static JTextField txtSurname;

	public static void showPopUpMenu(String id, String name, String surname) {
		userId = Integer.valueOf(id);
		deleteButton = new DeleteButton("Delete");
		updateButton = new UpdateButton("Update");
		txtName = new JTextField(name);
		txtSurname = new JTextField(surname);
		panel = new JPanel(new GridLayout(0, 1));
		panel.add(new JLabel("Name: "));
		panel.add(txtName);
		panel.add(new JLabel("Surname: "));
		panel.add(txtSurname);

		JOptionPane.showOptionDialog(null, panel, "Database Transactions", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, new Object[] { updateButton, deleteButton }, updateButton);

	}
}
