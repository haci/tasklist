package kanal.haci.tasklist.core.custom.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import kanal.haci.tasklist.core.connection.DBManager;
import kanal.haci.tasklist.core.connection.DBTransactionMessages;

public class DeleteButton extends JButton {

	private DBManager dbManager = new DBManager();

	private static final long serialVersionUID = 1L;

	public DeleteButton(String text) {
		setText(text);
		clickListener();
	}

	private void clickListener() {
		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					dbManager.deleteRecord("DELETE FROM user_datas WHERE id = '" + PopUpMenu.userId + "'");
					JOptionPane.showMessageDialog(null, DBTransactionMessages.DELETE);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

			}
		});
	}
}
