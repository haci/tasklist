package kanal.haci.tasklist.core.custom.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import kanal.haci.tasklist.core.connection.DBManager;
import kanal.haci.tasklist.core.connection.DBTransactionMessages;

public class SaveButton extends JButton {

	private DBManager dbManager = new DBManager();

	private static final long serialVersionUID = 1L;

	public SaveButton(String text) {
		setText(text);
		clickListener();
	}

	private void clickListener() {
		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					dbManager.insertRecord("INSERT INTO user_datas (id,name,surname,gender)VALUES (?,?,?,?)");
					JOptionPane.showMessageDialog(null, DBTransactionMessages.SAVE);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
}
