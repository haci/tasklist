package kanal.haci.tasklist.core.connection;

public class DBTransactionMessages {

	public static String SAVE = "Record is successfully inserted";
	public static String LIST = "Records are successfully listed";
	public static String UPDATE = "Record is successfully updated";
	public static String DELETE = "Record is successfully deleted";
}
