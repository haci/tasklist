package kanal.haci.tasklist.core.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

import kanal.haci.tasklist.core.custom.ui.ListFrameGui;
import kanal.haci.tasklist.core.custom.ui.MainFrameGui;

import com.mysql.jdbc.PreparedStatement;

public class DBManager {

	private Connection conn;
	private String userName = "root";
	private String password = "root";
	private String url = "jdbc:mysql://localhost/user_infos";

	public DBManager() {
		startConnection();
	}

	public void insertRecord(String sql_query) throws SQLException {
		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = (PreparedStatement) getConnection().prepareStatement(sql_query);
			preparedStatement.setInt(1, 0);
			preparedStatement.setString(2, MainFrameGui.txtName.getText().toString());
			preparedStatement.setString(3, MainFrameGui.txtSurname.getText().toString());
			preparedStatement.setString(4, MainFrameGui.chckbxMale.isSelected() ? "M" : "F");
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (getConnection() != null) {
				getConnection().close();
			}

		}
	}

	public void deleteRecord(String sql_query) throws SQLException {

		Statement statement = null;

		try {
			statement = getConnection().createStatement();

			statement.execute(sql_query);

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (statement != null) {
				statement.close();
			}

			if (getConnection() != null) {
				getConnection().close();
			}

		}

	}

	public void updateRecord(String sql_query) throws SQLException {

		Statement statement = null;

		try {
			statement = getConnection().createStatement();

			statement.execute(sql_query);

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (statement != null) {
				statement.close();
			}

			if (getConnection() != null) {
				getConnection().close();
			}

		}

	}

	public void retrieveDatas(String sql_query) throws SQLException {

		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = (PreparedStatement) getConnection().prepareStatement(sql_query);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Vector<Object> row = new Vector<Object>();

				for (int i = 1; i <= 4; i++) {
					row.addElement(rs.getObject(i));
				}

				((DefaultTableModel) ListFrameGui.model).addRow(row);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				preparedStatement.close();
			}

			if (getConnection() != null) {
				getConnection().close();
			}

		}
	}

	private void startConnection() {
		conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
		} catch (Exception e) {
			System.out.println("Exception found:  " + e.getMessage());
		}
	}

	@SuppressWarnings("unused")
	private void closeConnection() {
		try {
			conn.close();
		} catch (Exception e) {
			System.out.println("Connection close error  " + e.getMessage());
		}
	}

	public Connection getConnection() {
		return conn;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
