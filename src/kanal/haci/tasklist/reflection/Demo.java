package kanal.haci.tasklist.reflection;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;

public class Demo {

	static String name = "RefClass1";
	static String name2 = "RefClass2";

	public static void main(String[] args) {

		Class<RefClass1> ref1 = RefClass1.class;
		Class<RefClass2> ref2 = RefClass2.class;

		try {
			System.out.println("Enter the class name : ");
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String className = reader.readLine();
			if (className.equals(name)) {
				Object obj = ref1.newInstance();
				Method method = obj.getClass().getDeclaredMethod("writeOut", null);
				method.invoke(obj, null);
			}
			if (className.equals(name2)) {
				Object obj = ref2.newInstance();
				Method method = obj.getClass().getDeclaredMethod("writeOut", null);
				method.invoke(obj, null);
			}

		} catch (Exception e) {
			e.getMessage();
		}

	}

}
