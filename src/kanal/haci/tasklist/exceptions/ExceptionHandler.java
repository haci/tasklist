package kanal.haci.tasklist.exceptions;

public class ExceptionHandler {

	private double balance;

	public void deposit(double amount) {
		// deposit money to bank
		balance += amount;
	}

	public void withdraw(double amount) throws MyException {
		// if amount is less than balance, give money and update balance.
		// if amount is higher than balance,you don't have enough money message
		// will appear.
		if (amount <= balance) {
			balance -= amount;
		} else {
			throw new MyException(balance);
		}
	}

	public double getBalance() {
		return balance;
	}

}
