package kanal.haci.tasklist.exceptions;

public class BaseClass {

	private static double depositAmount = 500.00;
	private static double withdrawAmount = 600.00;

	public static void main(String[] args) {
		ExceptionHandler handler = new ExceptionHandler();
		System.out.println("Depositing $" + depositAmount + "...");
		handler.deposit(depositAmount);
		try {
			System.out.println("Trying to withdraw $" + withdrawAmount + "...");
			handler.withdraw(withdrawAmount);
			System.out.println("You have successfully withdrawn your money, your currenct balance is "
					+ handler.getBalance());
		} catch (MyException e) {
			System.err.println("Sorry, you don't have enough money to withdraw, your current balance is "
					+ e.getAmount());
		}
	}
}
