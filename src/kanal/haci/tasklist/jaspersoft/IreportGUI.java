package kanal.haci.tasklist.jaspersoft;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

public class IreportGUI {

	private JFrame frame;
	public static JTextField txtName;
	public static JTextField txtSurname;
	private JLabel lblGender;
	public static JCheckBox chckbxMale;
	public static JCheckBox chckbxFemale;
	public static JButton btnFilter;

	private Transactions actions = new Transactions();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IreportGUI window = new IreportGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public IreportGUI() {
		initializeGUI();
		initListeners();

	}

	private void initListeners() {
		actions.filterButtonListener();
	}

	private void initializeGUI() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(
				new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC, }, new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblIreportFiltering = new JLabel("Ireport Filtering");
		frame.getContentPane().add(lblIreportFiltering, "6, 2, center, default");

		JLabel lblName = new JLabel("Name");
		frame.getContentPane().add(lblName, "4, 4, left, default");

		txtName = new JTextField();
		frame.getContentPane().add(txtName, "6, 4, 3, 1, fill, default");
		txtName.setColumns(10);

		JLabel lblSurname = new JLabel("Surname");
		frame.getContentPane().add(lblSurname, "4, 6, left, default");

		txtSurname = new JTextField();
		frame.getContentPane().add(txtSurname, "6, 6, 3, 1, fill, default");
		txtSurname.setColumns(10);

		lblGender = new JLabel("Gender");
		frame.getContentPane().add(lblGender, "4, 8, left, default");

		chckbxMale = new JCheckBox("Male");
		frame.getContentPane().add(chckbxMale, "6, 8, center, default");

		chckbxFemale = new JCheckBox("Female");
		frame.getContentPane().add(chckbxFemale, "8, 8, left, default");

		btnFilter = new JButton("Filter");
		frame.getContentPane().add(btnFilter, "8, 10");

	}
}
