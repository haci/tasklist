package kanal.haci.tasklist.jaspersoft;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

public class Transactions {

	private Connection conn;
	private String userName = "root";
	private String password = "root";
	private String url = "jdbc:mysql://localhost/user_infos";
	private String path = "reports/user_reports.jasper";
	private String paramName = "NAME";
	private String paramSurname = "SURNAME";
	private Map<String, Object> params = new HashMap<>();

	public void filterButtonListener() {
		IreportGUI.btnFilter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					startConnection();

					params.clear();

					String name = IreportGUI.txtName.getText();
					String surname = IreportGUI.txtSurname.getText();

					params.put(paramName, name);
					params.put(paramSurname, surname);

					JasperPrint jPrint = JasperFillManager.fillReport(path, params, conn);
					JasperViewer.viewReport(jPrint, false);

					closeConnection();

				} catch (JRException e1) {
					e1.printStackTrace();
				}

			}
		});
	}

	public void startConnection() {
		conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, userName, password);
		} catch (Exception e) {
			System.out.println("Exception found:  " + e.getMessage());
		}
	}

	public void closeConnection() {
		try {
			conn.close();
		} catch (Exception e) {
			System.out.println("Connection close error  " + e.getMessage());
		}
	}

	public Connection getConnection() {
		return conn;
	}
}
