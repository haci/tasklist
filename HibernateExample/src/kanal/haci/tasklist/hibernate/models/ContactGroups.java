package kanal.haci.tasklist.hibernate.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contact_groups")
public class ContactGroups {
	@Id
	private int id;
	private String groupName;
	private String groupDescription;

	public int getId() {
		return id;
	}

	@Column(length = 4, name = "CONTACT_ID")
	@GeneratedValue
	public void setId(int id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	@Column(length = 25)
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	@Column(length = 250)
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

}
