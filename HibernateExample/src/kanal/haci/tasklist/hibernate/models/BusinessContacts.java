package kanal.haci.tasklist.hibernate.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "business_contacts")
public class BusinessContacts {
	@Id
	private int id;
	private int business_id;
	private int group_id;

	public int getId() {
		return id;
	}

	@Column(length = 4)
	@GeneratedValue
	public void setId(int id) {
		this.id = id;
	}

	public int getBusiness_id() {
		return business_id;
	}

	@Column(length = 4, unique = true)
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "BUSINESS_ID")
	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}

	public int getGroup_id() {
		return group_id;
	}

	@Column(length = 4, unique = true)
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CONTACT_ID")
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}

}
