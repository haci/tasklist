package kanal.haci.tasklist.hibernate.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "business_card")
public class BusinessCard {
	@Id
	private int id;
	private String name;
	private String description;

	public int getId() {
		return id;
	}

	@Column(length = 4, name = "BUSINESS_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@Column(length = 50)
	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	@Column(length = 250)
	public void setDescription(String description) {
		this.description = description;
	}

}
