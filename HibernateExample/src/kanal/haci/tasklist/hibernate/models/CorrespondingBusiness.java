package kanal.haci.tasklist.hibernate.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "corresponding_business")
public class CorrespondingBusiness {
	@Id
	private int id;
	private int business_id;
	private String phone_area_code;
	private String phone_number;

	public int getId() {
		return id;
	}

	@Column(length = 4)
	@GeneratedValue
	public void setId(int id) {
		this.id = id;
	}

	public int getBusiness_id() {
		return business_id;
	}

	@Column(length = 4)
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "BUSINESS_ID")
	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}

	public String getPhone_area_code() {
		return phone_area_code;
	}

	@Column(length = 5)
	public void setPhone_area_code(String phone_area_code) {
		this.phone_area_code = phone_area_code;
	}

	public String getPhone_number() {
		return phone_number;
	}

	@Column(length = 15)
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

}
