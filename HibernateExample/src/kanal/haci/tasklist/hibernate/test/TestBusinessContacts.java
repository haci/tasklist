package kanal.haci.tasklist.hibernate.test;

import kanal.haci.tasklist.hibernate.models.BusinessContacts;

import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

public class TestBusinessContacts {

	public static void main(String[] args) {
		Configuration config = new Configuration();
		config.addAnnotatedClass(BusinessContacts.class);
		config.configure("hibernate.cfg.xml");

		new SchemaExport(config).create(true, true);
	}

}
