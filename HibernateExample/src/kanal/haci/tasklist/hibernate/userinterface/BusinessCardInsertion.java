package kanal.haci.tasklist.hibernate.userinterface;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import kanal.haci.tasklist.hibernate.transactions.DBTransactions;

public class BusinessCardInsertion {

	private JFrame frame;
	private JTextField txtName;
	private JTextField txtDescription;
	private JButton btnInsert;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BusinessCardInsertion window = new BusinessCardInsertion();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public BusinessCardInsertion() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(
				new FormLayout(new ColumnSpec[] {
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						FormFactory.DEFAULT_COLSPEC,
						FormFactory.RELATED_GAP_COLSPEC,
						ColumnSpec.decode("default:grow"), }, new RowSpec[] {
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblName = new JLabel("Name");
		frame.getContentPane().add(lblName, "4, 4");

		txtName = new JTextField();
		frame.getContentPane().add(txtName, "6, 4, fill, default");
		txtName.setColumns(10);

		JLabel lblDescription = new JLabel("Description");
		frame.getContentPane().add(lblDescription, "4, 6");

		txtDescription = new JTextField();
		frame.getContentPane().add(txtDescription, "6, 6, fill, default");
		txtDescription.setColumns(10);

		btnInsert = new JButton("Insert");
		insertListener(btnInsert);
		frame.getContentPane().add(btnInsert, "6, 8");
	}

	public void insertListener(JButton button) {
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				DBTransactions.insertRecord(txtName.getText(),
						txtDescription.getText());
			}
		});
	}

}
