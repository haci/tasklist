package kanal.haci.tasklist.hibernate.transactions;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import kanal.haci.tasklist.hibernate.models.BusinessCard;

public class DBTransactions {

	private static SessionFactory sessionFactory;

	public static void insertRecord(String name, String description) {

		try {
			sessionFactory = new Configuration().configure("hibernate.cfg.xml")
					.buildSessionFactory();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();
		BusinessCard bCard = new BusinessCard();
		bCard.setName(name);
		bCard.setDescription(description);
		session.save(bCard);
		tx.commit();

	}
}
